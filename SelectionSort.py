def SelectionSort(arr):
    n = len(arr)
    for i in range(0, n-1):
        index_min = i
        for j in range(i+1, n):
            if arr[j] < arr[index_min]:
                index_min = j
        arr[index_min], arr[i] = arr[i], arr[index_min]

    return arr
#Uses
my_list = [75, 53445, 1, 1234, 87, 324, 237]

SelectionSort(my_list)

print(f"liste triée : {my_list}")

             


