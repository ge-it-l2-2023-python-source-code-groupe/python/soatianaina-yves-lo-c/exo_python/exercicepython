"""

# Cette fonction est l'interface publique pour 
#trier un tableau en utilisant l'algorithme de tri rapide. Elle appelle la fonction récursive
def quicksort(arr):
    # Appel initial à la fonction récursive
    quicksort_recursive(arr, 0, len(arr) - 1)

# Cette fonction est la fonction récursive réelle qui 
#divise le tableau en sous-tableaux, trie ces sous-tableaux, et les fusionne
def quicksort_recursive(arr, low, high):
    if low < high:
        # Trouver l'indice du pivot après partition
        pivot_index = partition(arr, low, high)

        # Trier récursivement les sous-tableaux avant et après le pivot
        quicksort_recursive(arr, low, pivot_index - 1)
        quicksort_recursive(arr, pivot_index + 1, high)

# Cette fonction effectue la partition du tableau autour d'un pivot. 
#Elle choisit le pivot (dans cet exemple, le dernier élément), place les éléments plus petits 
#à gauche et les éléments plus grands à droite du pivot, puis renvoie l'indice du pivot.
def partition(arr, low, high):
    # Choisir le pivot (dans cet exemple, le dernier élément)
    pivot = arr[high]

    # Indice pour l'élément le plus petit
    i = low - 1

    # Parcourir les éléments de l'indice "low" à "high-1"
    for j in range(low, high):
        # Si l'élément actuel est plus petit ou égal au pivot
        if arr[j] <= pivot:
            # Incrémenter l'indice de l'élément le plus petit
            i += 1
            # Échanger arr[i] et arr[j]
            arr[i], arr[j] = arr[j], arr[i]

    # Échanger arr[i+1] et arr[high] (le pivot)
    arr[i + 1], arr[high] = arr[high], arr[i + 1]

    # Retourner l'indice du pivot
    return i + 1

# Exemple d'utilisation
arr = [10, 7, 8, 9, 1, 5]
print("Tableau non trié:", arr)

quicksort(arr)

print("Tableau trié:", arr)
"""

                # ALGORITHM QUICK SORT

# Cette fonction appelle la fonction récursive
def quicksort(arr):
    # Appel initial à la fonction récursive
    quicksort_recursive(arr, 0, len(arr) - 1)

# Cette fonction est la fonction récursive réelle
def quicksort_recursive(arr, low, high):
    if low < high:

        # Trouver l'indice du pivot après partition
        pivot_index = partition(arr, low, high)

        # Trier récursivement les tableaux avant et après le pivot
        quicksort_recursive(arr, low, pivot_index - 1)
        quicksort_recursive(arr, pivot_index + 1, high)

# Cette fonction effectue la partition du tableau autour d'un pivot
def partition(arr, low, high):

    #choisir le pivot (dans cet exemple le dernier élément)
    pivot = arr[high]

    # Indice pour l'élément le plus petit
    i = low - 1

    # Parcourir les éléments de low à high - 1
    for j in range(low, high):

        # Si l'élément actuel est plus petit ou égal au pivot
        if arr[j] <= pivot:
            # Incrémenter l'indice de l'élément le plus petit
            i += 1
            # Echanger arr[i] et arr[j]
            arr[i], arr[j] = arr[j], arr[i]

    # Échanger arr[i + 1] et arr[high] (le pivot)
    arr[i+1], arr[high] = arr[high], arr[i+1]

    # Retourner l'indice du pivot
    return i + 1

# Exemple d'utilisation 
arr = [10, 7, 8, 9, 1, 5]
print(f"Tableau non trié : {arr}")

quicksort(arr)

print(f"tableau trié ; {arr}")