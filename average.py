notes = [14, 9, 8, 6, 12]
average = 0
sum = 0

for note in notes:
    sum = sum + note

average = sum / len(notes)

print("La moyenne est : {:.2f}".format(average))
