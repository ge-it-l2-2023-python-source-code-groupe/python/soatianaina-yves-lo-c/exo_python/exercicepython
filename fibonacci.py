""" 

                # SUITE DE FIBONACCI UTILISANT LA BOUCLE "WHILE"

# Définition d'une fonction nommé "fibonacci" avec un paramètre "n"
def fibonacci(n):
    
    # Initialisation d'une liste "fib_sequence" avec les 2 premiers terme de la suite
    fib_sequence = [0, 1]
    
    # Tant que la longueur de la séquence est inférieure à n 
    while len(fib_sequence) < n:
        
        # Calcul du prochain nombre de la suite en additionnant les 2 dernières termes 
        next_number = fib_sequence[-1] + fib_sequence[-2]
        
        # Ajout du prochain nombre à la séquence
        fib_sequence.append(next_number)

    # Renvoie la séquence de fibonacci complète
    return fib_sequence

# Définition de la valeur de "n" qui sera le nombre d'éléments de la suite souhaitée
n = 20

# Appel de la fonction "fibonacci" avec "n" comme argument et affectation du résultat dans "result"
result = fibonacci(n)

# Affichage de la séquence de fibonacci résultante
print(result)

"""

"""

                # Avec une fonction récursive

# Définition d'une fonction nommée "fibonacci" avec un paramètre "n"
def fibonacci(n):
    # Cas de base: si n est inférieur ou égale à zéro, retourner une liste vide
    if n <= 0 :
        return []
    # Cas spécial: si n est égale à 1, retourner une liste avec un seul élément, 0
    if n == 1 :
        return [0]
    # Cas spécial: si n est égale à 2, retourne une liste avec deux éléments, 0 et 1
    if n == 2 :
        return [0, 1]
    else:
        # Appel récursif de la fonction pour calculer les n-1 premiers termes de la suite
        fib_sequence = fibonacci(n - 1)

        # Calcul du prochain nombre en additionnant les 2 derniers
        next_number = fib_sequence[-1] + fib_sequence[-2]

        # Ajout du prochain nombre à la séquence
        fib_sequence.append(next_number)

        # Retourne la séquence de fibonacci complète
        return fib_sequence

# Définition de la valeur de "n" qui représente le nombre d'éléments de la suite souhaitée
n = 10

# Appel du fonction fibonacci avec "n" comme paramètre et stockage du résultat dans "result"
result = fibonacci(n)

# Affichage du résultat
print(result)

"""

                # AVEC DE LA BOUCLE FOR

# Définition d'une fonction apprelé "fibonacci" qui prend en paramètre "n"
def fibonacci(n):

    # Initialisation d'une séquence avec les deux premiers termes de la suite
    fib_sequence = [0, 1]

    # Pour les termes allant de 2 à n 
    for i in range(2, n) :

        # Calcul du prochain nombre
        next_number = fib_sequence[-1] + fib_sequence[-2]

        # Insertion du prochain nombre dans la liste "fib_sequence"
        fib_sequence.append(next_number)

    # Retourner les termes de la séquence
    return fib_sequence

# Affectation de la valeur de n
n = 10

# Appel de la fonction "fibonacci" avec paramètre "n" et affectation du résultat dans "result"
result = fibonacci(n)

# Affichage du résultat
print(result)
