N = int(input("Entrez un nombre de lignes (entier positif) : "))
tab = [[0 for val in range(N)] for val in range(N)]; print(f"tab : {tab}")

print("\nligne colonne")
for ligne in range(len(tab)):
    for colonne in range(len(tab[ligne])):
        print(f"{ligne+1:>4d}{colonne+1:>5d}")

print(f"{'-'*12}")

print("ligne colonne")
i=0
while i < len(tab):
    j = 0
    while j < len(tab[i]):
        j += 1
        print(f"{i+1:>4d}{j:>5d}")
    i += 1

