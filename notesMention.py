notes = [14, 9, 13, 15, 12]

print(f"La note minimum est {min(notes)} et la note maximum est {max(notes)}")
average = sum(notes) / len(notes)
print("La moyenne est : {:.2f}".format(average))

if (average >= 10 and average < 12):
    print("Mention passable")
if (average >= 12 and average < 14):
    print("Assez bien")
if (average >= 14):
    print("Mention passable")