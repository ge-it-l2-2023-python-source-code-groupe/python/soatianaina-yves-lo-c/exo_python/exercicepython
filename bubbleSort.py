               #ALGORITHME DE TRI À BULLE

# Définition d'une fonction appelée "bubble_sort" prenant une liste "arr" en paramètre
def bubble_sort(arr):
    # Obtenir la longueur de la liste arr
    n = len(arr)

    # Parcourrir tous les éléments de la liste
    for i in range(n):
        # Drapeau pour indiquer si des échanges ont été effectués pendant cette itération
        swapped = False

        # Parcourrir la liste de 0 à n-i-1
        # (n-i-1 car les derniers i sont déjà triés à chaque itération)
        for j in range(0, n-i-1):
            # Comparer les éléments adjacents
            if arr[j] > arr[j+1]:
                # Echanger les éléments s'ils sont dans le mauvais ordre 
                arr[j], arr[j+1] = arr[j+1], arr[j]
                swapped = True

    """ Si aucun échange n'a été effectué pendant cette itération, on sort directement de la boucle
        car la liste est déjà triée
       
       """
        if not swapped:
            break


# Exemple d'utilisation 
my_list = [64, 34, 25, 12, 22, 11, 90]

# Appeler la fonction "bubble_sort" pour trier la liste my_list
bubble_sort(my_list)

# Afficher la liste triée 
print("liste triée : ", my_list)


