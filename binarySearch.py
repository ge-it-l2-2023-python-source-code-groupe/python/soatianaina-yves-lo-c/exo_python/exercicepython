arr = [2, 3, 4, 10, 40]
target = 4
low = 0
high = len(arr) - 1


while(low <= high):

    mid = (low + high) // 2

    if arr[mid] == target:
        print(f"Indice trouvé à la position n° : {mid} ")
        break

    elif arr[mid] < target:
        low = mid + 1

    else:
        high = mid - 1

else:
    print("La cible n'est pas dans la liste.")

