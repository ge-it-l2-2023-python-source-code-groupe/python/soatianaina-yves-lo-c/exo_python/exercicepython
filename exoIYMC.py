"""
# Importez la fonction isprime de la bibliothèque sympy pour vérifier si un nombre est premier
from sympy import isprime

# Initialisez une liste de 12 premiers nombres premiers
primes = []
num = 2  # Le premier nombre premier

while len(primes) < 12:
    if isprime(num):
        primes.append(num)
    num += 1

# Calculez N en élevant chaque nombre premier à la sixième puissance et en les multipliant ensemble
N = 1
for prime in primes:
    N *= prime ** 6

# Affichez la valeur de N et le nombre de chiffres
print("N =", N)
print("Nombre de chiffres dans N :", len(str(N)))
"""

def is_prime(n):
    if n <= 1:
        return False
    if n == 2:
        return True
    if n % 2 == 0:
        return False
    i = 3
    while i * i <= n:
        if n % i == 0:
            return False
        i += 2
    return True

# Initialisez une liste de 12 premiers nombres premiers
primes = []
num = 2  # Le premier nombre premier

while len(primes) < 12:
    if is_prime(num):
        primes.append(num)
    num += 1

# Calculez N en élevant chaque nombre premier à la sixième puissance et en les multipliant ensemble
N = 1
for prime in primes:
    N *= prime ** 6

# Affichez la valeur de N et le nombre de chiffres
print("N =", N)
print("Nombre de chiffres dans N :", len(str(N)))

