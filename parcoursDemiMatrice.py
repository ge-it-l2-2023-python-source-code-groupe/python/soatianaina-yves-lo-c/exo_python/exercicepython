N = int(input("Entrez un nombre de lignes (entier positif): "))
tab = [[0 for val in range(N)] for val in range(N)]; print(f"tab : {tab}")

print("\nligne colonne")
i, counter = 0, 0
while i < len(tab):
    j = 0
    while j < len(tab[i]):
        while j > i and j != i:
            print(f"{i+1:>4d}{j+1:>5d}")
            counter += 1
            break
        j += 1
    i += 1
print(f"Pour une matrice {N}x{N}, on a parcouru {counter} cases")

#Cn = (n-1)² - Cn - 1
#Cn = (n-1) + Cn-1

