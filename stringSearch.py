                    # SUBSTRING SEARCH ALGORITHM
def search_substring(main_string, substring):

    # Obtenir la longueur de la chaîne principale et de la sous-chaîne
    main_length = len(main_string)
    substring_length = len(substring)

    # Parcourir la chaîne principale
    for i in range(main_length - substring_length + 1):


    # Vérifier si la sous-chaîne correspond aux caractères dans la chaîne principale
        match = True
        for j in range(substring_length):
            if main_string[i + j] != substring[j]:
                match = False
                break

        # Renvoyer l'indice de début si une correspondance est trouvée
        if match:
            return i

    # Si la sous-chaîne n'est pas trouvée, retourner -1
    return -1

# Exemple d'utilisation:
main_string = "Bonjour, comment ça va?"
substring = "comment"
result = search_substring(main_string, substring)

if result != -1:
    print(f"Sous-chaîne {substring} trouvée à l'indice {result}")
else:
    print(f"Sous-chaîne {substring} non trouvée dans la chaîne principale")
