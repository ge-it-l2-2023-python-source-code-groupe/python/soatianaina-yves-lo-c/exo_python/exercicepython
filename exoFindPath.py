from collections import defaultdict, deque

class Graph:

    def __init__(self):
        # Initialise un graphe en utilisant defaultdict pour stocker les listes des adjacences des sommets
        self.graph = defaultdict(list)
        
    def add_edge(self, u, v):
        # Ajout une arête non orientée entre le sommet u et v
        self.graph[u].append(v)
        self.graph[v].append(u)

    def find_path(self, start, end):
        #   Vérifie si le graphe contient le sommet de départ et d'arrivée
        if start not in self.graph or end not in self.graph:
            return None  # L'un des sommets n'est pas dans le graphe, pas de chemin possible

        # Utilise BFS pour trouver le chemin
        visited = set() # Stocker les sommets visités
        queue = deque([[start]])  # Crée une file d'attente avec le sommet de départ

        while queue:
            path = queue.popleft() # Récupérer le chemin de la file d'attente
            node = path[-1]  # Dernier sommet du chemin

            if node == end:
                return path  # Le chemin a été trouvé 

            if node not in visited:
                visited.add(node)  # Marque le sommet comme visité
                # Élargit le chemin en ajoutant les voisins non visités
                for neighbor in self.graph[node]:
                    if neighbor not in visited:
                        new_path = list(path) # Crée une nouvelle copie de chemin
                        new_path.append(neighbor)  # Ajoute le voisin au chemin
                        queue.append(new_path)  # Ajoute le nouveau chemin à la file d'attente

        return None # Aucun chemin trouvé

# Exemple d'utilisation 
if __name__ == "__main__":
    g = Graph()
    g.add_edge(0, 1)
    g.add_edge(0, 2)
    g.add_edge(1, 2)
    g.add_edge(2, 4)
    g.add_edge(3, 4)

    start_node = 0
    end_node = 4


    path = g.find_path(start_node, end_node)
    if path:
        print(f"Chemin trouvé entre {start_node} et {end_node} : {path}")
    else:
        print(f"Chemin non trouvé entre {start_node} et {end_node}")

