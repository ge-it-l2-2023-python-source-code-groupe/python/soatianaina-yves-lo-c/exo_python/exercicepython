listeAngle = [[48.6 ,53.4] ,[ -124.9 ,156.7] ,[ -66.2 ,-30.8] ,[ -58.8 ,-43.1] ,[ -73.9 ,-40.6] ,
[ -53.7 ,-37.5] ,[ -80.6 ,-26.0] ,[ -68.5 ,135.0] ,[ -64.9 ,-23.5] ,[ -66.9 ,-45.5] ,[ -69.6 ,-41.0] ,
[ -62.7 ,-37.5] ,[ -68.2 ,-38.3] ,[ -61.2 ,-49.1] ,[ -59.7 ,-41.1]]
phiParfait = -57
psiParfait = -47
compteHelice = 0
compteNonHelice = 0

for tab in listeAngle:
    if ((phiParfait-30)<=tab[0]<= (phiParfait+30) and (psiParfait-30)<=tab[1]<= (psiParfait+30)):
        compteHelice += 1
        print (f"{tab} est en hélice avec phi = {tab[0]} et psi = {tab[1]}")

    else:
        compteNonHelice += 1

print(f"Au total on a : {compteHelice} en hélice")
if compteHelice > compteNonHelice:
    print("Les hélices sont majoritaire")
elif compteHelice < compteNonHelice:
    print("Les non hélices sont majoritaires")
else:
    print("Il y a autant d'hélices et non hélices")













