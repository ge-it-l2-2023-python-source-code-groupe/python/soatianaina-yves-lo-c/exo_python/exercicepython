n = int(input("Entrez le nombre de paramètres : "))

fib_sequence = [0,1]

while len(fib_sequence) < n :

    next_number = fib_sequence[-1] + fib_sequence[-2]
    fib_sequence.append(next_number)

print(fib_sequence)