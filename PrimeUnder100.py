import math

n = 2
count = 0
while n < 100 :
    prime = True

    #for i in range (2, int(math.sqrt(n) + 1)):
    for i in range (2, n):
        if n % i == 0:
            prime = False
            break

    if prime:
        print(n, end = " ")
        count += 1

    n += 1

print(f"\nIl y a {count} nombres premiers inférieur à 100 ")
