                # ALGORITHME INSERTION_SORT
# Définir une fonction "insertion_sort" qui prend en paramètre une liste "arr"
def insertion_sort(arr):

    # Parcourez la liste à partir du deuxième élément, l'indice 1
    for i in range(1, len(arr)):

        # Stockez l'élément actuel que nous souhaitons stocker dans la liste triée
        current_element = arr[i]

        # Comparez l'élément actuel avec les éléments triés précédent
        j = i - 1
        while j >= 0 and current_element < arr[j]:
            # Si l'élément trié est plus grand que l'élément actuel 
            # Déplacez l'élément trié d'une position vers la droite
            arr[j+1] = arr[j]
            j -= 1

        # Placez l'élément actuel à sa position correcte dans la liste triée
        arr[j+1] = current_element

# Exemple d'utilisation 
my_list = [2, 9, 7, 3, 10, 54, 23, 11]

# Appeler la fonction 
insertion_sort(my_list)

# Afficher le résultat
print(f"La liste triée est : {my_list}")


