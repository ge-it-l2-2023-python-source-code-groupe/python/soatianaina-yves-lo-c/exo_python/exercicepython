brin = ["A", "R", "A", "W", "W", "A", "W", "A", "R", "W", "W", "R", "A", "G"]

countA= countR= countG= countW= 0

for i in range(len(brin)):
    if brin[i] == "A":
        countA += 1
    if brin[i] == "R":
        countR += 1
    if brin[i] == "G":
        countG += 1
    if brin[i] == "W":
        countW += 1

print(f"A = {countA}, R = {countR}, G = {countG}, W = {countW}")
