def creer_matrice_carree():
    try:
        taille = int(input("Entrez la taille de la matrice carrée : "))
        if taille <= 0:
            print("La taille de la matrice doit être un entier positif.")
            return None
        
        matrice = []
        print(f"Entrez les éléments pour une matrice {taille}x{taille} : ")

        for i in range(taille):
            ligne = []
            for j in range(taille):
                ligne = []
                for j in range(taille):
                    element = int(input(f"Entrez l'élément à la position [{i+1}][{j+1}] : "))
                    ligne.append(element)
                matrice.append(ligne)

        return matrice

    except ValueError:
        print("Veuillez entrer des valeurs numériques valides.")
        return None
        