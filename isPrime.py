import math

n = 2
count = 0
while n < 100:
    est_premier = True

    for i in range(2, int(math.sqrt(n)) + 1):
        if n % i == 0:
            est_premier = False
            break

    if est_premier:
        print(n, end=" ")
        count += 1

    n += 1

print(f"\nIl y a {count} nombres premier inférieur à 100 ")
